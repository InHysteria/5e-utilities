using Autofac;

namespace DungeonUtils
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(
                System.Reflection.Assembly.GetExecutingAssembly()
            );

            using (IContainer container = builder.Build())
            using (ILifetimeScope scope = container.BeginLifetimeScope())
                scope
                    .Resolve<IApplication>()
                    .Main()
                    .GetAwaiter()
                    .GetResult();
        }
    }
}
