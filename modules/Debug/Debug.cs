using System;

using Autofac;

namespace DungeonUtils.Debug
{
    public class Debug : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<NamePrinter>().As<IFeature>();
            builder.RegisterType<ConfigPrinter>().As<IFeature>();

            builder.RegisterType<PathTest1>().As<IFeature>();
            builder.RegisterType<PathTest2>().As<IFeature>();
            builder.RegisterType<PathTest3>().As<IFeature>();
            builder.RegisterType<PathTest4>().As<IFeature>();
            builder.RegisterType<PathTest5>().As<IFeature>();
        }
    }
}
