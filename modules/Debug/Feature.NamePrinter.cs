using System;
using System.Threading.Tasks;

namespace DungeonUtils.Debug
{
    public class NamePrinter : IFeature
    {
        public String Name { get; } = "Name Printer";
        public String Description { get; } = "Asks for a name and prints that name back out.";
        public String Path { get; } = "/DEBUG/Examples".Replace("/", Constants.PATH_SEPERATOR.ToString());

        private IInteractionService IO { get; }

        public NamePrinter(IInteractionService io)
        {
            IO = io;
        }

        public async Task Execute()
        {
            while (true)
            {
                String name = await IO.GetUserString("Please enter your name");
                await IO.DisplayString("The name you entered is: " + name);
            }
        }
    }
}
