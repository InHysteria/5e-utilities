using System;
using System.Threading.Tasks;

namespace DungeonUtils.Debug
{
    public class PathTest1 : IFeature
    {
        public String Name { get; } = "Path Test 1";
        public String Description { get; } = "Does nothing, this is here to test paths.";
        public String Path { get; } = "/DEBUG/PathTests/TestA/".Replace("/", Constants.PATH_SEPERATOR.ToString());

        public PathTest1() {}

        public async Task Execute() => await Task.Run(() => { });
    }

    public class PathTest2 : IFeature
    {
        public String Name { get; } = "Path Test 2";
        public String Description { get; } = "Does nothing, this is here to test paths.";
        public String Path { get; } = "/DEBUG/PathTests/TestA/".Replace("/", Constants.PATH_SEPERATOR.ToString());

        public PathTest2() {}

        public async Task Execute() => await Task.Run(() => { });
    }

    public class PathTest3 : IFeature
    {
        public String Name { get; } = "Path Test 3";
        public String Description { get; } = "Does nothing, this is here to test paths.";
        public String Path { get; } = "/DEBUG/PathTests/TestB/".Replace("/", Constants.PATH_SEPERATOR.ToString());

        public PathTest3() {}

        public async Task Execute() => await Task.Run(() => { });
    }

    public class PathTest4 : IFeature
    {
        public String Name { get; } = "Path Test 4";
        public String Description { get; } = "Does nothing, this is here to test paths.";
        public String Path { get; } = "/DEBUG/PathTests/TestB/".Replace("/", Constants.PATH_SEPERATOR.ToString());

        public PathTest4() {}

        public async Task Execute() => await Task.Run(() => { });
    }

    public class PathTest5 : IFeature
    {
        public String Name { get; } = "Path Test 5";
        public String Description { get; } = "Does nothing, this is here to test paths.";
        public String Path { get; } = "/DEBUG/PathTests/TestA/TestC".Replace("/", Constants.PATH_SEPERATOR.ToString());

        public PathTest5() {}

        public async Task Execute() => await Task.Run(() => { });
    }
}
