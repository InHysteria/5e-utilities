using System;
using System.Threading.Tasks;

namespace DungeonUtils.Debug
{
    public class ConfigPrinter : IFeature
    {
        public String Name { get; } = "Config Printer";
        public String Description { get; } = "Reads a config setting writting the retrieved value to the display.";
        public String Path { get; } = "/DEBUG/Examples".Replace("/", Constants.PATH_SEPERATOR.ToString());

        private IInteractionService IO { get; }
        private IConfigService Config { get; }

        public ConfigPrinter(IInteractionService io, IConfigService config)
        {
            IO = io;
            Config = config;
        }

        public async Task Execute()
        {
            while (true)
            {
                String path = await IO.GetUserString("Config setting");
                await IO.DisplayString("    = " + Config.GetSetting(path) ?? "<NULL>");
            }
        }
    }
}
