using System;

using Autofac;

namespace DungeonUtils.Console
{
    public class Console : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Application>().As<IApplication>();
            builder.RegisterType<ConsoleInteractionService>().As<IInteractionService>();
        }
    }
}
