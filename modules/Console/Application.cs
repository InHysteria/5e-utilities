using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DungeonUtils.Console
{
    public class Application : IApplication
    {
        public Dictionary<String, String[]> Paths { get; }
        public Dictionary<String, IFeature[]> Features { get; }

        private IInteractionService IO { get; }

        public Application(IInteractionService io, IEnumerable<IFeature> features)
        {
            IO = io;

            Paths = features
                .SelectMany(f => _explodePath(f.Path))
                .GroupBy(t => t.key)
                .ToDictionary(
                    g => g.Key,
                    g => g
                        .Select(t => t.value)
                        .Distinct()
                        .ToArray()
                );

            Features = features
                .GroupBy(f => f.Path)
                .ToDictionary(
                    g => g.Key,
                    g => g.ToArray()
                );
        }

        public async Task Main()
        {
            Stack<String> path = new Stack<String>();
            List<(String label, Func<Task> func)> options = new List<(String, Func<Task>)>();

            while (true)
            {
                options.Clear();

                String pathString = Constants.PATH_SEPERATOR + String.Join(Constants.PATH_SEPERATOR, path.Reverse());
                if (Paths.ContainsKey(pathString))
                    options.AddRange(Paths[pathString]
                        .Select(p => (
                            p + "..",
                            (Func<Task>)(() => Task.Run(() => path.Push(p)))
                        )));

                if (Features.ContainsKey(pathString))
                    options.AddRange(Features[pathString]
                        .Select(f => (
                            f.Name + " - " + f.Description,
                            (Func<Task>)f.Execute
                        )));

                if (path.Count > 0)
                    options.Add((
                        "..",
                        (Func<Task>)(() => Task.Run(() => path.Pop()))
                    ));

                await IO.Clear();
                await (await IO.GetUserChoice(
                    options,
                    "\n\n    Please select a tool to use",
                    t => t.label
                ))
                .func();
            }
        }

        private IEnumerable<(String key, String value)> _explodePath(String path)
        {
            String[] pathSegements = path
                .Trim(Constants.PATH_SEPERATOR)
                .Split(Constants.PATH_SEPERATOR);

            yield return (
                Constants.PATH_SEPERATOR.ToString(),
                pathSegements[0]
            );

            for (Int32 i = 0; i < pathSegements.Length - 1; i++)
                yield return (
                    Constants.PATH_SEPERATOR + String.Join(Constants.PATH_SEPERATOR.ToString(), pathSegements.Take(i+1)),
                    pathSegements[i+1]
                );

            /*
             For... /Debug/TestA/TestB/Example

             / -> Debug
             /Debug -> TestA
             /Debug/TestA -> TestB
             /Debug/TestA/TestB -> Example

            */
        }
    }
}
