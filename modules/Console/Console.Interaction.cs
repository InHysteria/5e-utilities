using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Autofac;

namespace DungeonUtils.Console
{
    public class ConsoleInteractionService : IInteractionService
    {
        public Task<String> GetUserString(String? prompt = null) => Task.Run(() =>
        {
            System.Console.Write((prompt ?? "Please enter a string") + ": ");
            return System.Console.ReadLine();
        });

        public async Task<Int32> GetUserInteger(String? prompt = null)
        {
            Int32 parsedInput;
            while (!Int32.TryParse(await GetUserString(prompt ?? "Please enter an integer"), out parsedInput))
            {}

            return parsedInput;
        }
        public async Task<Boolean> GetUserBoolean(String? prompt = null) =>
            (await GetUserString((prompt ?? "Yes or no?").TrimEnd(' ') + " [y/N]")).ToLower() == "y";

        public async Task<T> GetUserChoice<T>(
            IEnumerable<T> choices,
            String? prompt = null,
            Func<T, String>? presenter = null)
        {
            T[] choiceArray = choices.ToArray();
            Int32 selection = 0;

            while (true)
            {
                selection = await GetUserInteger(
$@"{prompt ?? ""}

        {String.Join("\n        ",
            choiceArray
                .Select(presenter ?? (o => o?.ToString() ?? ""))
                .Select((s,i) => (i + 1) + ". " + s)
        )}

    Select an option from above");

                if (0 < selection && selection <= choiceArray.Length)
                    break;
            }

            return choiceArray[selection - 1];
        }

        public Task Display<T>(T obj) => Task.Run(() => {
            if (obj == null) return;

            System.Console.WriteLine(
                String.Join("\n",
                    typeof(T)
                        .GetFields()
                        .Select(f => $"    {f.Name}: {f.GetValue(obj)}"))
            );
            System.Console.WriteLine(
                String.Join("\n",
                    typeof(T)
                        .GetProperties()
                        .Select(f => $"    {f.Name}: {f.GetValue(obj)}"))
            );
        });
        public Task DisplayString(String content) => Task.Run(() => System.Console.WriteLine(content));

        public Task WaitForContinue() => Task.Run(() => System.Console.ReadKey());
        public Task Clear() => Task.Run(() => System.Console.Clear());
    }
}
