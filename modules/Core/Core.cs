using System;

using Autofac;

namespace DungeonUtils.Core
{
    public class Core : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register(c => new ConfigService(".config")).As<IConfigService>();

            builder.RegisterType<RandomService>().As<IRandomService>();
            builder.RegisterType<TableService>().As<ITableService>();
            builder.RegisterType<DiceService>().As<IDiceService>();

            builder.RegisterType<DiceRoller>().As<IFeature>();
            builder.RegisterType<TableRoller>().As<IFeature>();
            builder.RegisterType<NPCGenerator>().As<IFeature>();
        }
    }
}
