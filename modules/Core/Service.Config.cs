using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DungeonUtils
{
    public class ConfigService : IConfigService
    {
        private JObject _config { get; }

        public ConfigService(String filepath)
        {
            _config = JObject.Parse(
                File.Exists(filepath)
                    ? File.ReadAllText(filepath)
                    : "{}"
            );
        }

        public String? GetSetting(String name)
        {
            Queue<String> path = new Queue<String>(name.Split('.'));
            JToken? node = _config;
            while (path.Count > 0 && node is JObject objectNode)
                node = objectNode?.Property(path.Dequeue())?.Value;

            return node?.ToString();
        }

        public IEnumerable<String> GetSettingsUnder(String name)
        {
            Queue<String> path = new Queue<String>(name.Split('.'));
            JObject? node = _config;
            while (path.Count > 0)
                node = node?.Property(path.Dequeue())?.Value as JObject;

            return node?.Properties().Select(p => p.Name) ?? Enumerable.Empty<String>();
        }
    }
}
