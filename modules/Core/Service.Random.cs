using System;

namespace DungeonUtils.Core
{
    public class RandomService : IRandomService
    {
        private Random _rng { get; }

        public RandomService()
        {
            _rng = new Random();
        }

        public Int32 Next() => _rng.Next();
        public Int32 Next(Int32 max) => _rng.Next(max);
        public Int32 Next(Int32 min, Int32 max) => _rng.Next(min, max);

        public Double NextDouble() => _rng.NextDouble();
    }
}
