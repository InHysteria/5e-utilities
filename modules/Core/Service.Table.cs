using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace DungeonUtils.Core
{
    public class TableService : ITableService
    {
        private IConfigService Config { get; }
        private IRandomService RNG { get; }

        private Dictionary<String, List<(Double weight, String name)>> _weightedTables;

        public TableService(IConfigService config, IRandomService random)
        {
            Config = config;
            RNG = random;

            _weightedTables = GetAvaliableTables()
                .Select(table => (
                    key: table,
                    value: _generateWeightedTable(table).ToList()
                ))
                .ToDictionary(
                    t => t.key,
                    t => t.value
                );
        }

        public IEnumerable<String> GetAvaliableTables() => Config.GetSettingsUnder(Constants.TABLE_ROOT);

        private static Regex _REPLACEMENT_PATTERN { get; } = new Regex(@"\$\{([^\}]*)\}", RegexOptions.Compiled);
        public String GetFromTable(String tableName, params (String replace, String with)[] replacements)
        {
            if (!_weightedTables.ContainsKey(tableName)) throw new Exception($"'{tableName}' doesn't exist.");

            Double roll = RNG.NextDouble();
            String result = _weightedTables[tableName].First(t => roll <= t.weight).name;

            foreach ((String replace, String with) in replacements)
                result = result.Replace(replace, with);

            return _REPLACEMENT_PATTERN.Replace(result, m => _weightedTables.ContainsKey(m.Groups[1].Value)
                ? GetFromTable(m.Groups[1].Value, replacements)
                : m.Groups[0].Value);
        }


        private IEnumerable<(Double weight, String name)> _generateWeightedTable(String table)
        {
            List<(Double weight, String name)> unadjusted = Config
                .GetSettingsUnder($"{Constants.TABLE_ROOT}.{table}")
                .Select(entry => (
                    weight: Double.TryParse(Config.GetSetting($"{Constants.TABLE_ROOT}.{table}.{entry}.{Constants.TABLE_WEIGHT}") ?? "1", out Double parsedWeight)
                        ? parsedWeight
                        : 1,
                    name: entry
                ))
                .ToList();

            Double rolling = 0;
            Double total = unadjusted.Sum(t => t.weight);

            foreach ((Double weight, String name) in unadjusted)
            {
                rolling += weight;
                yield return (rolling / total, name);
            }
        }
    }
}
