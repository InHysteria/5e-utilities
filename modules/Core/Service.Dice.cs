using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;

using Autofac;

namespace DungeonUtils.Core
{
    public class DiceService : IDiceService
    {
        private IRandomService RNG { get; }

        public DiceService(IRandomService rng)
        {
            RNG = rng;

            _operators = new Dictionary<String, (Int32, Boolean, ResolveDelegate)>
            {
                { @"+", (2, true, (ResolveDelegate)_add) },
                { @"-", (2, true, (ResolveDelegate)_sub) },
                { @"*", (3, true, (ResolveDelegate)_mul) },
                { @"/", (3, true, (ResolveDelegate)_div) },
                { @"%", (3, true, (ResolveDelegate)_mod) },
                { @"^", (4, false, (ResolveDelegate)_pow) },

                { @"d", (10, true, (ResolveDelegate)_roll) },
                { @"k", (10, true, (ResolveDelegate)_keepHighest) },
                { @"kh", (10, true, (ResolveDelegate)_keepHighest) },
                { @"kl", (10, true, (ResolveDelegate)_keepLowest) },
            };
        }

        public Int32 Roll(Int32 n, Int32 d) => Enumerable.Range(0, n)
            .Select(_ => RNG.Next(0, d) + 1)
            .Sum();

        private delegate IEnumerable<Double> ResolveDelegate(Stack<ResolveDelegate> stack, List<Int32>? rolledDice = null);
        private static Regex _TOKEN_PATTERN { get; } = new Regex($@"(\d+\.\d+|\d+|kh|kl|[\+\*-/%^dk]|[\(\)])", RegexOptions.Compiled);
        private Dictionary<String, (Int32 Precedence, Boolean LeftAssociative, ResolveDelegate Func)> _operators { get; }

        public Int32 Resolve(String formula, List<Int32>? rolledDice = null)
        {
            Stack<ResolveDelegate> output = new Stack<ResolveDelegate>();
            Stack<String> operatorStack = new Stack<String>();

            foreach (String token in _TOKEN_PATTERN
                .Matches(formula)
                .Select(m => m.Groups[1].Value))
            {
                if (_operators.ContainsKey(token))
                {
                    while  (operatorStack.Count > 0 &&
                            operatorStack.Peek() != "(" &&
                           (_operators[operatorStack.Peek()].Precedence > _operators[token].Precedence ||
                           (_operators[operatorStack.Peek()].Precedence == _operators[token].Precedence && _operators[token].LeftAssociative)))
                           output.Push(_operators[operatorStack.Pop()].Func);
                    operatorStack.Push(token);
                }
                else if (token == "(")
                    operatorStack.Push(token);
                else if (token == ")")
                {
                    while (operatorStack.Count > 0 && operatorStack.Peek() != "(")
                        output.Push(_operators[operatorStack.Pop()].Func);

                    if (operatorStack.Count == 0)
                        throw new ArgumentException(nameof(formula), "There is a problem with the provided formula. It has mismatched parentheses.");
                    else
                        operatorStack.Pop();
                }
                else if (Double.TryParse(token, out Double parsedToken))
                    output.Push((_1,_2) => new[] { parsedToken });
                else
                    throw new ArgumentException(nameof(formula), $"There is a problem with the provided formula. Unrecognised token '{token}'.");
            }

            while (operatorStack.Count > 0)
                if (operatorStack.Peek() == "(")
                    throw new ArgumentException(nameof(formula), "There is a problem with the provided formula. It has mismatched parentheses.");
                else
                    output.Push(_operators[operatorStack.Pop()].Func);

            if (output.Count == 0)
                throw new ArgumentException(nameof(formula), "There is a problem with the provided formula. The formula was empty or no tokens were found.");
            return (Int32)output.Pop()(output, rolledDice).Sum();
        }

        private IEnumerable<Double> _add(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Double right = output.Pop()(output, rolledDice).Sum();
            Double left = output.Pop()(output, rolledDice).Sum();

            System.Console.WriteLine($"_add({left}, {right})");
            yield return left + right;
        }
        private IEnumerable<Double> _mul(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Double right = output.Pop()(output, rolledDice).Sum();
            Double left = output.Pop()(output, rolledDice).Sum();

            System.Console.WriteLine($"_mul({left}, {right})");
            yield return left * right;
        }

        private IEnumerable<Double> _sub(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Double right = output.Pop()(output, rolledDice).Sum();
            Double left = output.Pop()(output, rolledDice).Sum();

            System.Console.WriteLine($"_sub({left}, {right})");
            yield return left - right;
        }
        private IEnumerable<Double> _div(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Double right = output.Pop()(output, rolledDice).Sum();
            Double left = output.Pop()(output, rolledDice).Sum();

            System.Console.WriteLine($"_div({left}, {right})");
            yield return left / right;
        }
        private IEnumerable<Double> _mod(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Double right = output.Pop()(output, rolledDice).Sum();
            Double left = output.Pop()(output).Sum();

            System.Console.WriteLine($"_mod({left}, {right})");
            yield return left % right;
        }
        private IEnumerable<Double> _pow(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Double right = output.Pop()(output, rolledDice).Sum();
            Double left = output.Pop()(output, rolledDice).Sum();

            System.Console.WriteLine($"_pow({left}, {right})");
            yield return (Double)System.Math.Pow(left, right);
        }

        private IEnumerable<Double> _roll(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Int32 size = (Int32)output.Pop()(output, rolledDice).Sum();
            Int32 no = (Int32)output.Pop()(output, rolledDice).Sum();

            IEnumerable<Int32> rolled = Enumerable.Range(0, no)
                .Select(_ => RNG.Next(0, size) + 1)
                .ToList();

            rolledDice?.AddRange(rolled);
            System.Console.WriteLine($"_roll({no}, {size})");
            return rolled.Cast<Double>();
        }
        private IEnumerable<Double> _keepHighest(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Int32 no = (Int32)output.Pop()(output, rolledDice).Sum();
            IEnumerable<Double> dice = output.Pop()(output, rolledDice);

            System.Console.WriteLine($"_keepHighest([{String.Join(",", dice)}], {no})");
            return dice
                .OrderByDescending(x => x)
                .Take(no);
        }
        private IEnumerable<Double> _keepLowest(Stack<ResolveDelegate> output, List<Int32>? rolledDice = null)
        {
            Int32 no = (Int32)output.Pop()(output, rolledDice).Sum();
            IEnumerable<Double> dice = output.Pop()(output, rolledDice);

            System.Console.WriteLine($"_keepLowest([{String.Join(",", dice)}], {no})");
            return dice
                .OrderBy(x => x)
                .Take(no);
        }
    }
}
