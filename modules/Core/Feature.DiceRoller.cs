using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils.Core
{
    public class DiceRoller : IFeature
    {
        public String Name { get; } = "Dice Roller";
        public String Description { get; } = "Rolls dice when given a formula.";
        public String Path { get; } = "/General".Replace("/", Constants.PATH_SEPERATOR.ToString());

        private IInteractionService IO { get; }
        private IDiceService Dice { get; }

        public DiceRoller(IInteractionService io, IDiceService dice)
        {
            IO = io;
            Dice = dice;
        }

        public async Task Execute()
        {
            await IO.Clear();
            await IO.DisplayString("\n    Dice Roller\n\n");
            while (true)
            {
                Int32 answer = 0;
                String formula = await IO.GetUserString("    Formula");
                List<Int32> formulaDice = new List<Int32>();
                try
                {
                    answer = Dice.Resolve(formula, formulaDice);
                }
                catch (Exception e)
                {
                    await IO.DisplayString($"There was an error. " + e.Message);
                    await IO.WaitForContinue();
                    continue;
                }

                if (formulaDice.Count > 0)
                    await IO.DisplayString($"     | {String.Join(", ", formulaDice)}");

                await IO.DisplayString($"     = {answer} \n");
            }
        }
    }
}
