using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils.Core
{
    public class NPCGenerator : IFeature
    {
        public String Name { get; } = "NPC";
        public String Description { get; } = "Generates an NPC character sheet.";
        public String Path { get; } = "/Generators".Replace("/", Constants.PATH_SEPERATOR.ToString());

        private IInteractionService IO { get; }
        private IConfigService Config { get; }
        private ITableService Table { get; }
        private IDiceService Dice { get; }

        public NPCGenerator(IInteractionService io, IConfigService config, ITableService table, IDiceService dice)
        {
            IO = io;
            Config = config;
            Table = table;
            Dice = dice;
        }

        public async Task Execute()
        {
            while (true)
            {
                String gender = Table.GetFromTable(Constants.GEN_GENDER_TABLE);
                String race = Table.GetFromTable(Constants.GEN_RACE_TABLE);
                String name = Table.GetFromTable(
                    Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_RACE_TABLE}.{race}.{Constants.GEN_RACE_NAME_TABLE}") ?? $"{race} Names",
                    ("|Gender|", Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_GENDER_TABLE}.{gender}.{Constants.GEN_GENDER_KEY}") ?? gender));

                String occupation = Table.GetFromTable(Constants.GEN_OCCUPATION_TABLE);
                String orientation = Table.GetFromTable(Constants.GEN_ORIENTATION_TABLE);
                String motivation = Table.GetFromTable(Constants.GET_MOTIVATION_TABLE);

                HashSet<String> likesDislikes = new HashSet<String>();
                while (likesDislikes.Count < 6)
                    likesDislikes.Add(Table.GetFromTable(Constants.GEN_LIKESDISLIKES_TABLE));

                var stats = new {
                    STR = 10 + (Dice.Roll(1,d:6) - 3) + Int32.Parse(Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_RACE_TABLE}.{race}.{Constants.GEN_RACE_STAT_MODIFIERS}.STR") ?? "0"),
                    DEX = 10 + (Dice.Roll(1,d:6) - 3) + Int32.Parse(Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_RACE_TABLE}.{race}.{Constants.GEN_RACE_STAT_MODIFIERS}.DEX") ?? "0"),
                    CON = 10 + (Dice.Roll(1,d:6) - 3) + Int32.Parse(Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_RACE_TABLE}.{race}.{Constants.GEN_RACE_STAT_MODIFIERS}.CON") ?? "0"),
                    INT = 10 + (Dice.Roll(1,d:6) - 3) + Int32.Parse(Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_RACE_TABLE}.{race}.{Constants.GEN_RACE_STAT_MODIFIERS}.INT") ?? "0"),
                    WIS = 10 + (Dice.Roll(1,d:6) - 3) + Int32.Parse(Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_RACE_TABLE}.{race}.{Constants.GEN_RACE_STAT_MODIFIERS}.WIS") ?? "0"),
                    CHA = 10 + (Dice.Roll(1,d:6) - 3) + Int32.Parse(Config.GetSetting($"{Constants.TABLE_ROOT}.{Constants.GEN_RACE_TABLE}.{race}.{Constants.GEN_RACE_STAT_MODIFIERS}.CHA") ?? "0")
                };

                await IO.Clear();
                await IO.DisplayString("\n    NPC Generator\n");
                await IO.Display(new {
                    Name = name,
                    Race = race,
                    Occupation = occupation,
                });
                await IO.DisplayString("");
                await IO.Display(stats);
                await IO.DisplayString("");
                await IO.Display(new {
                    Gender = gender,
                    Orientation = orientation,
                    Motivation = motivation,
                    Likes = String.Join(", ", likesDislikes.Take(3)),
                    Hates = String.Join(", ", likesDislikes.Skip(3)),
                });
                await IO.DisplayString("");
                await IO.WaitForContinue();
            }
        }
    }
}
