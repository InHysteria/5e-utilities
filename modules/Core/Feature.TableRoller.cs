using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils.Core
{
    public class TableRoller : IFeature
    {
        public String Name { get; } = "Table Roller";
        public String Description { get; } = "Picks a random option from a table defined in the config.";
        public String Path { get; } = "/General".Replace("/", Constants.PATH_SEPERATOR.ToString());

        private IInteractionService IO { get; }
        private ITableService Table { get; }

        public TableRoller(IInteractionService io, ITableService table)
        {
            IO = io;
            Table = table;
        }

        public async Task Execute()
        {
            while (true)
            {
                await IO.Clear();
                await IO.DisplayString("\n    Table Roller\n\n");
                String table = await IO.GetUserChoice(
                    Table.GetAvaliableTables(),
                    prompt: "    Select a table from the list below."
                );

                await IO.DisplayString("        = " + Table.GetFromTable(table));
                await IO.WaitForContinue();
            }
        }
    }
}
