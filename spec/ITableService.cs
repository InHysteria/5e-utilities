using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils
{
    public interface ITableService
    {
        IEnumerable<String> GetAvaliableTables();
        String GetFromTable(String tableName, params (String replace, String with)[] replacements);
    }
}
