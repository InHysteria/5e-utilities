using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils
{
    public interface IRandomService
    {
        Int32 Next();
        Int32 Next(Int32 max);
        Int32 Next(Int32 min, Int32 max);

        Double NextDouble();
    }
}
