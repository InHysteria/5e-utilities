using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils
{
    public interface IDiceService
    {
        Int32 Roll(Int32 n, Int32 d); // Roll(2,d:8) + 4;

        Int32 Resolve(String formula, List<Int32>? rolledDice = null); // Resolve("2d8 + 4");
    }
}
