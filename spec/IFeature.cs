using System;
using System.Threading.Tasks;

namespace DungeonUtils
{
    public interface IFeature
    {
        String Name { get; }
        String Description { get; }
        String Path { get; }

        Task Execute();
    }
}
