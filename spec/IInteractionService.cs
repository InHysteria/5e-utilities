using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils
{
    public interface IInteractionService
    {
        Task<String> GetUserString(String? prompt = null);
        Task<Int32> GetUserInteger(String? prompt = null);
        Task<Boolean> GetUserBoolean(String? prompt = null);

        Task<T> GetUserChoice<T>(
            IEnumerable<T> choices,
            String? prompt = null,
            Func<T, String>? presenter = null);

        Task Display<T>(T obj);
        Task DisplayString(String content);
        
        Task WaitForContinue();
        Task Clear();
    }
}
