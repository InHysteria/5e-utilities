using System;
using System.Threading.Tasks;

namespace DungeonUtils
{
    public interface IApplication
    {
        Task Main();
    }
}
