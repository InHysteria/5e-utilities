using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace DungeonUtils
{
    public interface IConfigService
    {
        String? GetSetting(String name);
        IEnumerable<String> GetSettingsUnder(String name);
    }
}
