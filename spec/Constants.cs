using System;

namespace DungeonUtils
{
    public static class Constants
    {
        public const Char PATH_SEPERATOR = '/';

        public const String TABLE_ROOT = "Tables";
        public const String TABLE_WEIGHT = "__table_weight__";

        public const String GEN_RACE_TABLE = "Races";
        public const String GEN_RACE_NAME_TABLE = "__generator_name_table__";
        public const String GEN_RACE_STAT_MODIFIERS = "Stat Modifiers";
        public const String GEN_GENDER_TABLE = "Gender / Sex";
        public const String GEN_GENDER_KEY = "__gender_key__";
        public const String GEN_OCCUPATION_TABLE = "Occupations";
        public const String GEN_ORIENTATION_TABLE = "Orientations";
        public const String GET_MOTIVATION_TABLE = "Motivations";
        public const String GEN_LIKESDISLIKES_TABLE = "Likes/Dislikes";
    }
}
